# Yieldify API Test

The purpose of this project can be found [here]

# Features

The features in this programme are the following: 
- Allows user to pick which directory they want to watch (it will default to ./ if one isn't picked)
- If given a .gz file, it will be unzipped and placed in the same directory
- Given the file is of the correct format, being (date, time, user_id, url, IP, user_agent_string), it will then be parsed and stored into an in memory event summary, a ConcurrentLinkedQueue was used for this, as getting specific indexes of the collection isn't important and the data structure is thread safe.
- If the file has already been processed, it won't be processed again, this is achieved by storing the md5 hash of files already processed in a HashSet
- If the printTopFives cli argument was included in running the programme, the top 5 stats outlined in the instruction document will be printed.
- After the file is parsed, the API's can now be queried to get results

# API Requests

**Show Browser Stats**
----
  Returns json data about the proportion of different browsers used based on the events as a percentage
  
  Results will be global if no dates are specified in the params, otherwise the results will be between the range of dates given

* **URL**

  /stats/browser/:fromDate&:toDate

* **Method:**

  `GET`

*  **URL Params**

   **Optional:**
 
   `fromDate=['yyyy-mm-ddThh:mm:ss']`
   `toDate=['yyyy-mm-ddThh:mm:ss']`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"lastFileProcessed":"2018-01-01T00:00:00","browserStats":{"Safari":"1","Firefox":"5"}}`
  
    OR

  * **Code:** 200 <br />
    **Content:** `{"lastFileProcessed":"Nothing Processed"}`

* **Access API locally:**

  ```
  http://localhost:4567/stats/browser/
  http://localhost:4567/stats/browser/2011-11-11T11:11:11&2012-11-11T11:11:11
  ```
**Show Device Stats**
----
  Returns json data about the proportion of different devices used based on the events as a percentage.
  
   Results will be global if no dates are specified in the params, otherwise the results will be between the range of dates given.

* **URL**

  /stats/device/:fromDate&:toDate

* **Method:**

  `GET`

*  **URL Params**

   **Optional:**
 
   `fromDate=['yyyy-mm-ddThh:mm:ss']`
   `toDate=['yyyy-mm-ddThh:mm:ss']`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"lastFileProcessed":"2018-01-01T00:00:00","deviceStats":{"Robot":"1","Laptop":"5"}}`
  
    OR

  * **Code:** 200 <br />
    **Content:** `{"lastFileProcessed":"Nothing Processed"}`

* **Access API locally:**

  ```
  http://localhost:4567/stats/device/
  http://localhost:4567/stats/device/2011-11-11T11:11:11&2012-11-11T11:11:11
  ```
  
**Show OS Stats**
----
  Returns json data about the proportion of different OS used based on the events as a percentage
  
  Results will be global if no dates are specified in the params, otherwise the results will be between the range of dates given

* **URL**

  /stats/os/:fromDate&:toDate

* **Method:**

  `GET`

*  **URL Params**

   **Optional:**
 
   `fromDate=['yyyy-mm-ddThh:mm:ss']`
   `toDate=['yyyy-mm-ddThh:mm:ss']`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"osStats":{"Android 6.1":"1","iOS 10.0":"5"}, "lastFileProcessed":"2018-01-01T00:00:00"}`
  
    OR

  * **Code:** 200 <br />
    **Content:** `{"lastFileProcessed":"Nothing Processed"}`

* **Access API locally:**

  ```
  http://localhost:4567/stats/os/
  http://localhost:4567/stats/os/2011-11-11T11:11:11&2012-11-11T11:11:11
  ```
# How to Run

Running the programme depends on having maven installed, if maven is installed, the following command will run the programme: 
```sh
$ mvn clean install compile exec:java@[default || printTopFives]
```
If the default parameter is chosen, the programme will not print out the stats from Task A to command line and if the printTopFives parameter is chosen it will.

# Dependencies

- The library used to decipher countries and cities from IP Addresses was provided by [MaxMind] library. A .dat file provided by their website allows this to occur offline, which makes the parsing a lot quicker as opposed to using an API request to request the country and city of each IP Address, which would be far more time and power consuming.
- The UserAgentUtils library provided by [BitWalker] was used to decipher the user agent
- The new [SparkJava] micro framework was used to implement the API's


   [SparkJava]: <http://sparkjava.com/>
   [BitWalker]: <https://www.bitwalker.eu/software/user-agent-utils>
   [MaxMind]: <https://dev.maxmind.com/geoip/legacy/geolite/>
   [here]: <https://docs.google.com/document/d/1dz9WmbUGrdvJ_XAbKVEa939wsWGzs4gYtXTCirks4Uo/edit#heading=h.apier1i5b2q5>