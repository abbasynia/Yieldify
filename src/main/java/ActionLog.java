import org.joda.time.DateTime;

public class ActionLog {
        private final DateTime dateTime;
        private final String userId;
        private final String country;
        private final String city;
        private final String browserFamily;
        private final String osFamily;

        private final String device;

        public ActionLog(DateTime dateTime, String userId, String country, String city, String browserFamily, String osFamily, String device) {
            this.dateTime = dateTime;
            this.userId = userId;
            this.country = country;
            this.city = city;
            this.browserFamily = browserFamily;
            this.osFamily = osFamily;
            this.device = device;
        }



    public String getDevice() {
            return device;
        }

        public DateTime getDateTime() {
            return dateTime;
        }

        public String getUserId() {
            return userId;
        }

        public String getCountry() {
            return country;
        }

        public String getCity() {
            return city;
        }

        public String getBrowserFamily() {
            return browserFamily;
        }

        public String getOsFamily() {
            return osFamily;
        }
}
