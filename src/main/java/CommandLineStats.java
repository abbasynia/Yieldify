import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

public class CommandLineStats {
    private final Collection<ActionLog> actionLogSummary;

    public CommandLineStats(Collection<ActionLog> actionLogSummary) {
        this.actionLogSummary = actionLogSummary;
    }

    public void printTopFiveStats() {

        System.out.println("Top 5 Countries based on number of events: " + getTopFiveCountriesBasedOnEvents());

        System.out.println("Top 5 Cities based on number of events: " + getTopFiveCitiesBasedOnEvents());

        System.out.println("Top 5 Browsers based on number of unique users: " + getTopFiveBrowsersUsedForUniqueUserIds());

        System.out.println("Top 5 Operating systems based on number of unique users: " + getTopFiveOsUsedForUniqueUserIds());
    }

    public List<Map.Entry<String, Integer>> getTopFiveOsUsedForUniqueUserIds() {
        List<Map.Entry<String, Integer>> result = getTopFiveForVariableToSet(actionLogSummary.stream()
                .filter(p -> !p.getOsFamily().equals("Unknown"))
                .collect(Collectors.groupingBy(ActionLog::getOsFamily,
                        Collectors.mapping(ActionLog::getUserId, Collectors.toSet()))));
        return result.size() > 5 ? result.subList(0, 5) : result;
    }

    public List<Map.Entry<String, Integer>> getTopFiveBrowsersUsedForUniqueUserIds() {
        List<Map.Entry<String, Integer>> result = getTopFiveForVariableToSet(actionLogSummary.stream()
                .filter(p -> !p.getBrowserFamily().equals("Unknown"))
                .collect(Collectors.groupingBy(ActionLog::getBrowserFamily,
                        Collectors.mapping(ActionLog::getUserId, Collectors.toSet()))));
        return result.size() > 5 ? result.subList(0, 5) : result;
    }

    public List<Map.Entry<String, Long>> getTopFiveCitiesBasedOnEvents() {
        List<Map.Entry<String, Long>> result = getTopFiveFor(actionLogSummary.stream()
                .filter(p -> !p.getCity().equals("Unknown"))
                .collect(Collectors.groupingBy(ActionLog::getCity, Collectors.counting())));
        return result.size() > 5 ? result.subList(0, 5) : result;
    }

    public List<Map.Entry<String, Long>> getTopFiveCountriesBasedOnEvents() {
        List<Map.Entry<String, Long>> result = getTopFiveFor(actionLogSummary.stream()
                .filter(p -> !p.getCountry().equals("Unknown"))
                .collect(Collectors.groupingBy(ActionLog::getCountry, Collectors.counting())));
        return result.size() > 5 ? result.subList(0, 5) : result;
    }

    private List<Map.Entry<String, Integer>> getTopFiveForVariableToSet(Map<String, Set<String>> variableToNumberOfEvents) {
        return variableToNumberOfEvents
                .entrySet().stream()
                .collect(toMap(Map.Entry::getKey, e -> e.getValue().size()))
                .entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toList());
    }

    private List<Map.Entry<String, Long>> getTopFiveFor(Map<String, Long> variableToNumberOfEvents) {
        return variableToNumberOfEvents
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toList());
    }
}
