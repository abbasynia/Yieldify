import com.maxmind.geoip.Location;
import com.maxmind.geoip.LookupService;
import eu.bitwalker.useragentutils.UserAgent;
import org.apache.commons.codec.digest.DigestUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import spark.utils.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;

import static java.util.stream.Collectors.toMap;

public class FileWatchAndParse implements Runnable {

    private static final Logger LOGGER = Logger.getLogger( FileWatchAndParse.class.getName() );

    private final String path;
    private final boolean printTopFives;
    private final Collection<ActionLog> actionLogSummary = new ConcurrentLinkedQueue<>();
    private final Set<String> filesProcessed = new HashSet<>();
    private volatile String lastDateTimeProcessed = "Nothing Processed";
    private final DecimalFormat decimalFormat = new DecimalFormat("###.##");


    public FileWatchAndParse(String path, boolean printTopFives) {
        this.path = path + "/";
        this.printTopFives = printTopFives;
    }

    public void run() {
        Path myDir = Paths.get(path);

        try {
            LookupService lookupService = null;
            try {
                lookupService = new LookupService(new File("./GeoLiteCity.dat"), LookupService.GEOIP_MEMORY_CACHE);
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "GeoLiteCity.dat is not present in current directory. Please put GeoLiteCity.dat in current directory as it is necessary for parsing an IP address");
                LOGGER.log(Level.SEVERE,"System terminating");
                System.exit(1);
            }
            WatchService watchService = myDir.getFileSystem().newWatchService();

            myDir.register(
                    watchService,
                    StandardWatchEventKinds.ENTRY_CREATE);

            WatchKey key;
            LOGGER.log(Level.INFO, "Ready to listen for files in " + path);
            while ((key = watchService.take()) != null) {
                for (WatchEvent<?> event : key.pollEvents()) {
                    String fileName = path + event.context();
                    if (fileName.endsWith(".gz")) {
                        unzipFile(fileName);
                    } else {
                        try (Stream<String> stream = Files.lines(Paths.get(fileName));
                             FileInputStream fileInputStream = new FileInputStream(new File(fileName))) {
                            String md5Hash = DigestUtils.md5Hex(IOUtils.toByteArray(fileInputStream));
                            if (filesProcessed.contains(md5Hash)) {
                                LOGGER.log(Level.INFO, "File has already been processed");
                            } else {
                                parseFileAndAddToEventSummary(lookupService, stream);
                                filesProcessed.add(md5Hash);

                                LOGGER.log(Level.INFO, "File processed");
                                LOGGER.log(Level.INFO, "API's are ready to use");

                                if (printTopFives) {
                                    CommandLineStats printer = new CommandLineStats(actionLogSummary);
                                    printer.printTopFiveStats();
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                key.reset();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
        Used tutorial from https://examples.javacodegeeks.com/core-java/io/fileinputstream/decompress-a-gzip-file-in-java-example/
        to help adapt this method
    */
    private void unzipFile(String fileName) {
        try (FileInputStream fileIn = new FileInputStream(fileName);
             GZIPInputStream gZIPInputStream = new GZIPInputStream(fileIn)) {

            byte[] buffer = new byte[1024];

            String outputFileName = fileName.split(".gz")[0];
            FileOutputStream fileOutputStream = new FileOutputStream(outputFileName);
            int bytes_read;

            while ((bytes_read = gZIPInputStream.read(buffer)) > 0) {
                fileOutputStream.write(buffer, 0, bytes_read);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void parseFileAndAddToEventSummary(LookupService lookupService, Stream<String> stream) {
        stream.forEach(lineInFile -> {
            String[] line = lineInFile.split("\t");
            DateTimeFormatter f = DateTimeFormat.forPattern("yyyy-MM-ddHH:mm:ss");
            DateTime dateTime = f.parseDateTime(line[0] + line[1]);
            this.lastDateTimeProcessed = dateTime.toString();
            Location location = lookupService.getLocation(line[4]);
            String country;
            String city;
            if (location != null) {
                country = location.countryName != null ? location.countryName : "Unknown";
                city = location.city != null ? location.city : "Unknown";
            } else {
                country = "Unknown";
                city = "Unknown";
            }
            UserAgent agent = UserAgent.parseUserAgentString(line[5]);
            String browserFamily = agent.getBrowser().getGroup().getName();
            String osFamily = agent.getOperatingSystem().getName();
            String device = agent.getOperatingSystem().getDeviceType().getName();
            actionLogSummary.add(new ActionLog(dateTime, line[2], country, city, browserFamily, osFamily, device));
        });
    }

    public Map<String, String> getBrowserStats() {
        return getGlobalStatsForCollector(Collectors.groupingBy(ActionLog::getBrowserFamily, Collectors.counting()));
    }

    public Map<String, String> getBrowserStatsBetween(DateTime fromDateTime, DateTime toDateTime) {
        Collector<ActionLog, ?, Map<String, Long>> actionLogMapCollector = Collectors.groupingBy(ActionLog::getBrowserFamily, Collectors.counting());
        return getStatsBetweenForCollector(fromDateTime, toDateTime, actionLogMapCollector);
    }

    public Map<String, String> getOsStats() {
        return getGlobalStatsForCollector(Collectors.groupingBy(ActionLog::getOsFamily, Collectors.counting()));
    }

    public Map<String, String> getOsStatsBetween(DateTime fromDateTime, DateTime toDateTime) {
        return getStatsBetweenForCollector(fromDateTime, toDateTime, Collectors.groupingBy(ActionLog::getOsFamily, Collectors.counting()));
    }

    public Map<String, String> getDeviceStats() {
        return getGlobalStatsForCollector(Collectors.groupingBy(ActionLog::getDevice, Collectors.counting()));
    }

    public Map<String, String> getDeviceStatsBetween(DateTime fromDateTime, DateTime toDateTime) {
        return getStatsBetweenForCollector(fromDateTime, toDateTime, Collectors.groupingBy(ActionLog::getDevice, Collectors.counting()));
    }

    private Map<String, String> getGlobalStatsForCollector(Collector<ActionLog, ?, Map<String, Long>> actionLogMapCollector) {
        return actionLogSummary.stream()
                .collect(actionLogMapCollector)
                .entrySet().stream()
                .collect(toMap(Entry::getKey, v -> decimalFormat.format((v.getValue().doubleValue() / actionLogSummary.size()) * 100) + "%"));
    }

    private Map<String, String> getStatsBetweenForCollector(DateTime fromDateTime, DateTime toDateTime, Collector<ActionLog, ?, Map<String, Long>> actionLogMapCollector) {
        Map<String, Long> filteredActivitySummary = actionLogSummary.stream()
                .filter(p -> p.getDateTime().isAfter(fromDateTime) && p.getDateTime().isBefore(toDateTime))
                .collect(actionLogMapCollector);

        int sizeOfFilteredActivitySummary = filteredActivitySummary.size();

        return filteredActivitySummary.entrySet().stream()
                .collect(toMap(Entry::getKey, v -> decimalFormat.format((v.getValue().doubleValue() / sizeOfFilteredActivitySummary) * 100) + "%"));
    }

    public String getLastDateTimeProcessed() {
        return lastDateTimeProcessed;
    }
}