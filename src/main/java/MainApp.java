import java.util.Scanner;
import java.util.logging.Logger;

public class MainApp {
    private static final Logger LOGGER = Logger.getLogger( MainApp.class.getName() );

    public static void main (String args[]) {
        String path;
        try (Scanner reader = new Scanner(System.in)) {
            System.out.println("Enter path where files will be stored");
            path = reader.nextLine();
        } catch (Exception e) {
            LOGGER.warning("Input not taken, instead path will be current dir");
            path = ".";
        }

        boolean printTopFives = args.length > 0 && args[0].equals("printTopFives");
        FileWatchAndParse fileWatchAndParse = new FileWatchAndParse(path, printTopFives);
        new Thread(fileWatchAndParse).start();
        Runnable statsResource = new StatsResources(fileWatchAndParse);
        new Thread(statsResource).start();
    }
}