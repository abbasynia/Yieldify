import org.joda.time.DateTime;
import org.json.simple.JSONObject;
import spark.Spark;

import static spark.Spark.get;
import static spark.Spark.path;

public class StatsResources implements Runnable {

    private FileWatchAndParse fileWatchAndParse;

    public StatsResources(FileWatchAndParse fileWatchAndParse) {
        this.fileWatchAndParse = fileWatchAndParse;
    }

    public void run() {
        path("/stats", () -> {
            path("/browser", () -> {
                get("/", (req, res) -> {
                    JSONObject returnObject = new JSONObject();
                    returnObject.put("lastFileProcessed", fileWatchAndParse.getLastDateTimeProcessed());
                    returnObject.put("browserStats", fileWatchAndParse.getBrowserStats());
                    return returnObject;
                });

                get("/:range", (req, res) -> {
                    String[] timeRange = req.params(":range").split("&");
                    JSONObject returnObject = new JSONObject();
                    returnObject.put("lastFileProcessed", fileWatchAndParse.getLastDateTimeProcessed());
                    returnObject.put("browserStats", fileWatchAndParse.getBrowserStatsBetween(DateTime.parse(timeRange[0]), DateTime.parse(timeRange[1])));
                    return returnObject;
                });
            });

            path("/os", () -> {
                get("/", (req, res) -> {
                    JSONObject returnObject = new JSONObject();
                    returnObject.put("lastFileProcessed", fileWatchAndParse.getLastDateTimeProcessed());
                    returnObject.put("osStats", fileWatchAndParse.getOsStats());
                    return returnObject;
                });

                get("/:range", (req, res) -> {
                    String[] timeRange = req.params(":range").split("&");
                    JSONObject returnObject = new JSONObject();
                    returnObject.put("lastFileProcessed", fileWatchAndParse.getLastDateTimeProcessed());
                    returnObject.put("osStats", fileWatchAndParse.getOsStatsBetween(DateTime.parse(timeRange[0]), DateTime.parse(timeRange[1])));
                    return returnObject;
                });
            });

            path("/device", () -> {
                get("/", (req, res) -> {
                    JSONObject returnObject = new JSONObject();
                    returnObject.put("lastFileProcessed", fileWatchAndParse.getLastDateTimeProcessed());
                    returnObject.put("deviceStats", fileWatchAndParse.getDeviceStats());
                    return returnObject;
                });

                get("/:range", (req, res) -> {
                    String[] timeRange = req.params(":range").split("&");
                    JSONObject returnObject = new JSONObject();
                    returnObject.put("lastFileProcessed", fileWatchAndParse.getLastDateTimeProcessed());
                    returnObject.put("deviceStats", fileWatchAndParse.getDeviceStatsBetween(DateTime.parse(timeRange[0]), DateTime.parse(timeRange[1])));
                    return returnObject;
                });
            });
        });
    }

    public void stop() {
        Spark.stop();
    }
}
