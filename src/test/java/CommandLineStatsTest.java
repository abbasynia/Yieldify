import com.google.common.collect.ImmutableList;
import org.joda.time.DateTime;
import org.junit.Test;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

public class CommandLineStatsTest {

    @Test
    public void test() throws Exception {
        Collection<ActionLog> activityLog = new ArrayList<>();
        activityLog.add(new ActionLog(DateTime.parse("2018-01-10T17:00:00"), "userId1", "UK", "London", "Safari", "Apple", "iPad"));
        activityLog.add(new ActionLog(DateTime.parse("2018-01-10T17:00:00"), "userId2", "UK", "London", "Chrome", "Windows", "Laptop"));
        activityLog.add(new ActionLog(DateTime.parse("2018-01-10T17:00:00"), "userId3", "UK", "Manchester", "Safari", "Apple", "iPad"));
        activityLog.add(new ActionLog(DateTime.parse("2018-01-10T17:00:00"), "userId4", "France", "Paris", "Chrome", "Windows", "Laptop"));
        activityLog.add(new ActionLog(DateTime.parse("2018-01-10T17:00:00"), "userId5", "Spain", "Madrid", "Firefox", "Android", "Mobile"));
        activityLog.add(new ActionLog(DateTime.parse("2018-01-10T17:00:00"), "userId5", "USA", "NY", "Firefox", "Android", "Mobile"));
        activityLog.add(new ActionLog(DateTime.parse("2018-01-10T17:00:00"), "userId5", "USA", "NY", "Firefox", "Android", "Mobile"));

        CommandLineStats commandLineStats = new CommandLineStats(activityLog);

        assertEquals(commandLineStats.getTopFiveBrowsersUsedForUniqueUserIds(), ImmutableList.of(
                new SimpleEntry<>("Safari",2),
                new SimpleEntry<>("Chrome",2),
                new SimpleEntry<>("Firefox",1)
        ));

        assertEquals(commandLineStats.getTopFiveCountriesBasedOnEvents(), ImmutableList.of(
                new SimpleEntry<>("UK",3L),
                new SimpleEntry<>("USA",2L),
                new SimpleEntry<>("France",1L),
                new SimpleEntry<>("Spain",1L)
        ));

        assertEquals(commandLineStats.getTopFiveOsUsedForUniqueUserIds(), ImmutableList.of(
                new SimpleEntry<>("Apple",2),
                new SimpleEntry<>("Windows",2),
                new SimpleEntry<>("Android",1)
        ));

        assertEquals(commandLineStats.getTopFiveCitiesBasedOnEvents(), ImmutableList.of(
                new SimpleEntry<>("London",2L),
                new SimpleEntry<>("NY",2L),
                new SimpleEntry<>("Madrid",1L),
                new SimpleEntry<>("Manchester",1L),
                new SimpleEntry<>("Paris",1L)
        ));
    }
}
