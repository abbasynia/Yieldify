import com.google.common.collect.ImmutableMap;
import com.maxmind.geoip.LookupService;
import org.joda.time.DateTime;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;



public class FileWatchAndParseTest {

    private static FileWatchAndParse fileWatchAndParse;

    @BeforeClass public static void
    setup() throws IOException{
        fileWatchAndParse = new FileWatchAndParse("./", true);
        LookupService lookupService = new LookupService(new File("./GeoLiteCity.dat"), LookupService.GEOIP_MEMORY_CACHE);

        Collection<String> fileData = new ArrayList<>();

        fileData.add("2014-10-13\t" +
                "20:00:59\t" +
                "3431a66a9249dafe00f8d3b5825dd420aed3e85c\t" +
                "http://b8baac7e1775b630e66648c0ea76b43968c78969/a2e2213a381ee60bf0c9e87ec3a530641befbe49\t" +
                "88.104.129.0\t" +
                "Mozilla/5.0 (iPad; CPU OS 8_0_2 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12A405 Safari/600.1.4");

        fileData.add("2014-10-12\t" +
                "20:00:59\t" +
                "1431a66a9249dafe00f8d3b5825dd420aed3e85c\t" +
                "http://18baac7e1775b630e66648c0ea76b43968c78969/a2e2213a381ee60bf0c9e87ec3a530641befbe49\t" +
                "77.104.129.0\t" +
                "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.101 Safari/537.36");
        fileData.add("2014-10-11\t" +
                "20:00:59\t" +
                "2431a66a9249dafe00f8d3b5825dd420aed3e85c\t" +
                "http://d8baac7e1775b630e66648c0ea76b43968c78969/a2e2213a381ee60bf0c9e87ec3a530641befbe49\t" +
                "88.104.129.0\t" +
                "Mozilla/5.0 (iPad; CPU OS 8_0_2 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12A405 Safari/600.1.4");

        fileWatchAndParse.parseFileAndAddToEventSummary(lookupService, fileData.stream());

    }

    @Test
    public void
    returnsCorrectGlobalStatsWhenGivenEvents() {
        assertEquals(fileWatchAndParse.getBrowserStats(), ImmutableMap.of("Safari", "66.67%", "Chrome", "33.33%"));
        assertEquals(fileWatchAndParse.getDeviceStats(), ImmutableMap.of("Computer", "33.33%", "Tablet", "66.67%"));
        assertEquals(fileWatchAndParse.getOsStats(), ImmutableMap.of("iOS 8 (iPad)", "66.67%", "Windows 7", "33.33%"));
    }

    @Test
    public void
    returnsCorrectStatsInBetweenGivenDateWhenGivenEvents() {
        assertEquals(fileWatchAndParse.getBrowserStatsBetween(DateTime.parse("2014-10-11T19:00:00"), DateTime.parse("2014-10-12T23:59:00")), ImmutableMap.of("Safari", "50%", "Chrome", "50%"));
        assertEquals(fileWatchAndParse.getDeviceStatsBetween(DateTime.parse("2014-10-11T19:00:00"), DateTime.parse("2014-10-12T23:59:00")), ImmutableMap.of("Computer", "50%", "Tablet", "50%"));
        assertEquals(fileWatchAndParse.getOsStatsBetween(DateTime.parse("2014-10-11T19:00:00"), DateTime.parse("2014-10-12T23:59:00")), ImmutableMap.of("iOS 8 (iPad)", "50%", "Windows 7", "50%"));
    }
}
