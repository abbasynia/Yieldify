import com.google.common.collect.ImmutableMap;
import org.joda.time.DateTime;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import java.net.URI;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StatsResourcesTest {

     private static final FileWatchAndParse mockedFileWatcher = mock(FileWatchAndParse.class);
     private static final StatsResources statsResources = new StatsResources(mockedFileWatcher);

    @BeforeClass
    public static void setup() {
        statsResources.run();
        when(mockedFileWatcher.getLastDateTimeProcessed()).thenReturn("2018-01-01T00:00:00");
        when(mockedFileWatcher.getBrowserStats()).thenReturn(ImmutableMap.of("Safari", "1", "Firefox", "5"));
        when(mockedFileWatcher.getBrowserStatsBetween(DateTime.parse("2011-11-11T11:11:11"), DateTime.parse("2012-11-11T11:11:11"))).thenReturn(ImmutableMap.of("Opera", "1", "curl", "2"));
        when(mockedFileWatcher.getDeviceStats()).thenReturn(ImmutableMap.of("Robot", "1", "Macbook", "5", "Mobile", "3"));
        when(mockedFileWatcher.getDeviceStatsBetween(DateTime.parse("2011-11-11T11:11:11"), DateTime.parse("2012-11-11T11:11:11"))).thenReturn(ImmutableMap.of("Android", "3", "iPhone", "2"));
        when(mockedFileWatcher.getOsStats()).thenReturn(ImmutableMap.of("Android 8.1", "1", "Android 6.1", "5"));
        when(mockedFileWatcher.getOsStatsBetween(DateTime.parse("2011-11-11T11:11:11"), DateTime.parse("2012-11-11T11:11:11"))).thenReturn(ImmutableMap.of("Android 8.1", "1", "Android 6.1", "2"));
    }

    @AfterClass
    public static void cleanup() {
        statsResources.stop();
    }

    @Test
    public void returnsGlobalBrowserStats() {

        Client client = ClientBuilder.newBuilder().build();
        Response response = client.target(URI.create("http://localhost:4567/stats/browser/"))
                .request()
                .get();

        assertEquals(response.getStatus(), 200);
        assertEquals(response.readEntity(String.class), "{\"lastFileProcessed\":\"2018-01-01T00:00:00\",\"browserStats\":{\"Safari\":\"1\",\"Firefox\":\"5\"}}");
    }

    @Test
    public void returnsBrowserStatsBetweenDates() {
        Client client = ClientBuilder.newBuilder().build();
        Response response = client.target(URI.create("http://localhost:4567/stats/browser/2011-11-11T11:11:11&2012-11-11T11:11:11"))
                .request()
                .get();

        assertEquals(response.getStatus(), 200);
        assertEquals(response.readEntity(String.class), "{\"lastFileProcessed\":\"2018-01-01T00:00:00\",\"browserStats\":{\"Opera\":\"1\",\"curl\":\"2\"}}");
    }

    @Test
    public void returnsGlobalDeviceStats() {
        Client client = ClientBuilder.newBuilder().build();
        Response response = client.target(URI.create("http://localhost:4567/stats/device/"))
                .request()
                .get();

        assertEquals(response.getStatus(), 200);
        assertEquals(response.readEntity(String.class),"{\"lastFileProcessed\":\"2018-01-01T00:00:00\",\"deviceStats\":{\"Robot\":\"1\",\"Macbook\":\"5\",\"Mobile\":\"3\"}}");
    }

    @Test
    public void returnsDeviceStatsBetweenDates() {

        Client client = ClientBuilder.newBuilder().build();
        Response response = client.target(URI.create("http://localhost:4567/stats/device/2011-11-11T11:11:11&2012-11-11T11:11:11"))
                .request()
                .get();

        assertEquals(response.getStatus(), 200);
        assertEquals(response.readEntity(String.class), "{\"lastFileProcessed\":\"2018-01-01T00:00:00\",\"deviceStats\":{\"Android\":\"3\",\"iPhone\":\"2\"}}");
    }

    @Test
    public void returnsGlobalOsStats() {

        Client client = ClientBuilder.newBuilder().build();
        Response response = client.target(URI.create("http://localhost:4567/stats/os/"))
                .request()
                .get();

        assertEquals(response.getStatus(), 200);
        assertEquals(response.readEntity(String.class), "{\"osStats\":{\"Android 8.1\":\"1\",\"Android 6.1\":\"5\"},\"lastFileProcessed\":\"2018-01-01T00:00:00\"}");
    }

    @Test
    public void returnsOsStatsBetweenDates() {

        Client client = ClientBuilder.newBuilder().build();
        Response response = client.target(URI.create("http://localhost:4567/stats/os/2011-11-11T11:11:11&2012-11-11T11:11:11"))
                .request()
                .get();

        assertEquals(response.getStatus(), 200);
        assertEquals(response.readEntity(String.class), "{\"osStats\":{\"Android 8.1\":\"1\",\"Android 6.1\":\"2\"},\"lastFileProcessed\":\"2018-01-01T00:00:00\"}");
    }

    @Test
    public void throwsErrorWhenGivenWrongDateFormat() {

        Client client = ClientBuilder.newBuilder().build();
        Response response = client.target(URI.create("http://localhost:4567/stats/device/1stJune&2ndJune"))
                .request()
                .get();

        assertEquals(response.getStatus(), 500);
    }
}
